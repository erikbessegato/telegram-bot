class User:
    
    def __init__(self, chat_id):
        self.chat_id = chat_id
        self.name = None
        self.surname = None
        self.role = None
        
    def asArray(self):
        arr = (None,self.chat_id,self.name,self.surname,self.role)  
        return arr
